import React from "react";
import ReactDom from "react-dom";

import PlayerTable from "./components/player-table"

const App = () => {
    return (
        <>
            <div className={"container"}>
                <PlayerTable/>
            </div>
        </>
    );
};

const nodeApp = document.querySelector("#app");
nodeApp ? ReactDom.render(<App/>, nodeApp) : console.warn("Not find nodeApp");
