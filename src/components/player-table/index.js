import React from "react";

import Table from "../ui/table";
import Input from "../ui/input"
import Button from "../ui/button";
import Row from "../ui/row";
import CheckBox from "../ui/checkbox";
import PLayerDialog from "../player-dialog";
import PlayerModal from "../player-modal";

import BaseApi from "../../api/index"

import {sortArrayObjBySomting} from "../../utils/sort";

class PlayerTable extends React.Component {

    state = {
        isLoading: true,
        fieldsHeader: [
            {
                title: "ID",
                filterType: "id",
                sortable: true,
                isSorting: null,
            },
            {
                title: "Имя",
                filterType: "name",
                sortable: true,
                isSorting: null,
            },
            {
                title: "Уровень",
                filterType: "level",
                sortable: true,
                isSorting: null,
            },
            {
                title: "Онлайн",
                filterType: "online",
                sortable: true,
                isSorting: null,
            }
        ],
        dialog: {
            position: 0,
            isActive: false,
        },
        clickedPlayer: null,
        modalPlayer: {
            isActive: false,
            player: null,
        }
    };

    async componentDidMount() {
        let players = await BaseApi.getPlayers();
        players = players.map((player) => {
            return {...player, hide: false, active: true};
        });

        this.setState({
            players,
            isLoading: false
        });
    }


    onTableClick = (e) => {
        const node = e.target;
        const isFilter = node.dataset.filterType;
        const isPlayerRow = node.dataset.player;

        if (isFilter !== undefined) {
            this.filterClickController(isFilter);
        }

        if (isPlayerRow !== undefined) {
            this.playerClickController(e.nativeEvent, isPlayerRow);
        }
    };

    filterClickController(idFilter) {
        const fieldFilterIndex = this.state.fieldsHeader.findIndex((item) => item.filterType == idFilter);
        const fieldsHEader = [...this.state.fieldsHeader].map((item) => {
            item.filterType !== idFilter
                ? item.isSorting = null
                : item.isSorting = !item.isSorting;
        });

        this.setState({
            players: sortArrayObjBySomting(this.state.players, this.state.fieldsHeader[fieldFilterIndex].isSorting, idFilter),
            fieldsHEader
        });
    }

    playerClickController(event, idPlayer) {
        let gap = 180;
        if (window.outerWidth < 850) {
            gap = 270;
        }

        const position = event.layerY + gap;
        this.setState({
            dialog: {
                position,
                isActive: true,
            },
            clickedPlayer: parseInt(idPlayer)
        })
    }

    itemDialogClick = (e) => {
        const node = e.target;
        const type = node.dataset.type;

        const indexPlayer = this.state.players
            .findIndex(player => player.id === this.state.clickedPlayer);

        if (type === "hide") {
            const newPlayer = {...this.state.players[indexPlayer], active: false};
            const newPlayers = [...this.state.players.slice(0, indexPlayer), newPlayer, ...this.state.players.slice(indexPlayer + 1)];
            this.setState({
                players: newPlayers
            })
        }

        if (type === "show") {
            this.setState({
                modalPlayer: {
                    isActive: true,
                    player: {...this.state.players[indexPlayer]},
                }
            })
        }

        this.setState({
            dialog: {
                position: 0,
                isActive: false,
            },
            clickedPlayer: null,
        })
    };

    closeModal = () => {
        this.setState({
            modalPlayer: {
                isActive: false,
                player: null,
            }
        })
    };

    onChangeCheckbox = (e) => {
        const value = e.target.checked;
        const newPlayers = this.state.players.map((item) => {
            if (!item.online) {
                return {...item, hide: value};
            }
            return item;
        });

        this.setState({
            players: newPlayers,
            onlyOnline: value,
        }, () => {
            if (this.state.filterInputVal !== undefined && !value) {
                this.onInput({target: {value: this.state.filterInputVal}});
            }
        });
    };

    onInput = (e) => {
        const value = e.target.value.toUpperCase();
        const newPlayers = this.state.players.map((item) => {
            const name = item.name.toUpperCase();
            if (name.startsWith(value) && item.active) {
                return {...item, hide: false};
            }
            return {...item, hide: true};
        });

        this.setState({
            players: newPlayers,
            filterInputVal: value,
        }, () => {
            if (this.state.onlyOnline) {
                this.onChangeCheckbox({target: {checked: this.state.onlyOnline}});
            }
        });
    };

    showAllPlayers = () => {
        const newPlayers = this.state.players.map((item) => {
            if (!item.active) {
                return {...item, active: true, hide: false};
            }
            return item;
        });

        this.setState({
            players: newPlayers,
        },() => {
          if (this.state.filterInputVal !== undefined) {
            this.onInput({target: {value: this.state.filterInputVal}});
          } else if (this.state.onlyOnline) {
            this.onChangeCheckbox({target: {checked: this.state.onlyOnline}});
          }
        })
    };


    render() {
        const {isLoading, players, fieldsHeader, dialog, modalPlayer} = this.state;

        if (isLoading) {
            return <p>Загружаем данные таблицы</p>
        }

        return (
            <>
                <Row type={"row_filter"}>
                    <div className={"field field_player-name"}>
                        <div className={"field__label"}>Имя</div>
                        <Input
                            onChange={this.onInput}
                        />
                    </div>
                    <div className={"field field_player-online"}>
                        <div className={"field__label"}>Онлайн</div>
                        <CheckBox
                            onChange={this.onChangeCheckbox}
                        />
                    </div>
                    <Button
                        onClick={this.showAllPlayers}
                        type={"btn_filter"}
                        title={"Показать всех"}
                    />
                </Row>
                <Table
                    onClick={this.onTableClick}
                    fieldsHeader={fieldsHeader}
                    fieldsBody={players}
                />
                <PLayerDialog
                    params={dialog}
                    clickItem={this.itemDialogClick}
                />
                <PlayerModal
                    closeModal={this.closeModal}
                    params={modalPlayer}
                />
            </>
        );
    }
};

export default PlayerTable;
