import React from "react";
import Dialog from "../ui/dialog";

const playerMenu = [
    {
        title: "Показать профиль",
        type: "show"
    },
    {
        title: "Скрыть игрока",
        type: "hide"
    }
];

const PlayerDialog = React.memo(({params, clickItem}) => {

    return <Dialog
        clickItem={clickItem}
        isActive={params.isActive}
        position={params.position}
        menu={playerMenu}
        className={"dialog_players"}
    />
});

export default PlayerDialog;
