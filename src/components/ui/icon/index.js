import React from "react";

const Icon = React.memo(({type = "", className = ""}) => {
    return (
        <div className={`icon ${type} ${className}`}></div>
    );
});

export default Icon;
