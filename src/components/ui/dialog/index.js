import React from "react";

const Dialog = ({className,menu,position = 0,isActive = false, clickItem = ()=>{}}) => {

  const activeClass = isActive ? "dialog_active" : "";

  const inlineStyle = {
    top: `${position}px`
  };

  return (
      <ul className={`dialog ${className} ${activeClass}`} style={inlineStyle}>
        {menu.map(({title, type}) => {
          return (
              <li key={type}
                  onClick={clickItem}
                  data-type={type}
                  className={"dialog__item"}>{title}
              </li>
          );
        })}
      </ul>
  );
};

export default Dialog;
