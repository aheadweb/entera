import React from "react";

const Input = React.memo(({onChange}) => {
    return (
        <input type="text" onChange={onChange} className={"input"}/>
    );
});


export default Input;
