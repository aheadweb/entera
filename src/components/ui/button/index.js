import React from "react";

const Button = React.memo(({type, title, onClick = ()=>{} }) => {
  return (
      <button
          onClick={onClick}
          className={`btn ${type}`}>{title}
      </button>
  );
});

export default Button;
