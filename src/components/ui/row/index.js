import React from "react";

const Row = React.memo(({children, type}) => {
    return (
        <div className={`row ${type}`}>
            {children}
        </div>
    );
});

export default Row;
