import React from "react";
import Icon from "../icon";

const Table = (props) => {

  const {fieldsHeader, fieldsBody, onClick} = props;
  
  return (
    <>
      <div className={"table table_players"}
        onClick={onClick}
      >
        <div className={"table__header"}>
          <div className={"table__row"}>
            {fieldsHeader.map(({title, sortable, filterType, isSorting}) => {
              return (
                <div className={"table__col"} key={title}>
                  {title}
                  {sortable 
                    ? <div className={"table__filter"} data-filter-type={filterType}>
                        <span className={`icon icon-filter-arrow table__filter-arrow ${isSorting === false ? "icon-filter-arrow_active" : ""}`}></span>
                        <span className={`icon icon-filter-arrow icon-filter-arrow_bottom table__filter-arrow ${isSorting === true ? "icon-filter-arrow_active" : ""}`}></span>
                      </div>
                    : ""  
                  }
              </div>
              );
            })}       
          </div>
        </div>
        <div className={"table__body"}>
          {fieldsBody.map(({id, name, level, online, hide, active}) => {
            return (
              <div className={`table__row ${hide || !active ? "table__row_hidden": ""} `} key={id} data-player={id}>
                <div className={"table__col"}>{id}</div>
                <div className={"table__col"}>{name}</div>
                <div className={"table__col"}>
                  {new Array(level).fill(1).map((_,i) => {
                    return <Icon key={i}
                    type={"icon-star"}
                    className={"table__rating"}/>
                  })}     
                </div>
                <div className={"table__col"}>
                  <Icon
                      type={`${online ? "icon-online" : "icon-online icon-online_disable"}`}
                    />
                </div>
              </div>)
            })}
        </div>
      </div>
    </>
  );
};

export default Table;