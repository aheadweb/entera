import React from "react";

const CheckBox = React.memo(({onChange}) => {
    return (
        <div className={"checkbox-wrapper"}>
            <input id="online" onChange={onChange} className={"checkbox"} type="checkbox"/>
            <label htmlFor="online" className={"checkbox-label"}></label>
        </div>
    );
});

export default CheckBox;
