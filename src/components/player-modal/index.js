import React from "react";

import Button from "../ui/button";
import Icon from "../ui/icon";

const PlayerModal = React.memo(({params, closeModal}) => {
    let {isActive, player} = params;
    if (player === null) {
        player = {}
    }
    const {name, online, level} = player;
    return (
        <div className={`modal ${isActive ? "modal_active" : ""}`}>
            <div className="modal__left">{name}</div>
            <div className="modal__right">
                <div className="modal__player-status">{online ? "Онлайн" : "Оффлайн"}</div>
                <div className="modal__player-rating">
                    {new Array(level).fill().map((_, i) => {
                        return (
                            <Icon
                                key={i}
                                type={"icon-star icon-star_big"}
                                className={"modal__player-start"}
                            />)
                    })}
                </div>
                <Button
                    onClick={closeModal}
                    type="btn_modal"
                    title={"Закрыть"}
                />
            </div>
        </div>
    );
});

export default PlayerModal;
