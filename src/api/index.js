import players from "../players.json";

class BaseApi {
    constructor(fakePlayers) {
        this.players = fakePlayers;
    }

    getPlayers() {
        return new Promise((resolve) => {
            //fake fetch players
            // in this case always resolve
            setTimeout(() => {
                resolve(this.players);
            }, 2000)
        });
    }
}

export default new BaseApi(players);
