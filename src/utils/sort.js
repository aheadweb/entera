export function sortArrayObjBySomting (array,upOrDown,key) {
  return upOrDown 
    ? array.sort((objA,objB) => compare(objA[key],objB[key]))
    : array.sort((objA,objB) => compare(objB[key],objA[key]))
}

function compare(a, b) {

  if (a < b) {
    return -1;
  }

  if (a > b) {
    return 1;
  }
  
  return 0;
}