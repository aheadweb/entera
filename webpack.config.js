const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');

module.exports = (_,args) => {

  const isDev = args.mode === "development";

  const optimization = (isDev) => {
    if(!isDev) {
      return [
        new TerserPlugin({}), 
        new OptimizeCSSAssetsPlugin({})
      ]
    }

    return [];
  }

  return {
    context: path.resolve(__dirname, "src"),
    entry: ["@babel/polyfill","./index.js"],
    devtool: "eval-source-map",
    output: {
      path: path.resolve(__dirname, "release"),
      filename: '[name].[contenthash].js'
    },
    devServer: {
      contentBase: path.join(__dirname, "release"),
      open: true,
      overlay: true,
      port: 8081
    },
    optimization: {
      minimizer: optimization(isDev),
      splitChunks: {
        chunks: "all",
      }
    },
    module: {
      rules: [
        {
          test: /\.less$/,
          use: [
            {
              loader: MiniCssExtractPlugin.loader,
              options: {

              }
            },
            "css-loader",
            "less-loader"
          ]
        },
        {
          test: /\.(js|jsx)$/,
          exclude: /node_modules/,
          use: {
            loader: "babel-loader"
          }
        }
      ]
    },
    plugins: [
      new CleanWebpackPlugin(),
      new HtmlWebpackPlugin({
        template: "./index.html"
      }),
      new MiniCssExtractPlugin({
        filename: "[name].[contenthash].css"
      }),
    ]
  };
}